/**
 * CKEditor plugin for inserting media embeds.
 *
 * Doesn't add any extra markup.
 *
 * Based on http://ckeditor.com/addon/mediaembed
 */

(function() {
  CKEDITOR.plugins.add('ckeditor_simple_media_embed', {
    icons: 'ckeditor_simple_media_embed',
    hidpi: true,
    init: function(editor) {
      CKEDITOR.dialog.add('SimpleMediaEmbedDialog', function(instance) {
        return {
          title: Drupal.t('Embed Media'),
          minWidth: 550,
          minHeight: 200,
          contents: [{
            id: 'iframe',
            expand: true,
            elements: [{
                id: 'embedArea',
                type: 'textarea',
                label: Drupal.t('Paste Embed Code Here'),
                autofocus: 'autofocus'
              }]
          }],
          onOk: function() {
            var div = instance.document.createElement('div');
            div.setHtml(this.getContentElement('iframe', 'embedArea').getValue());
            instance.insertElement(div);
          }
        };
      });

      editor.addCommand('SimpleMediaEmbed', new CKEDITOR.dialogCommand('SimpleMediaEmbedDialog', {
        allowedContent: 'iframe[*]'
      }));

      editor.ui.addButton('SimpleMediaEmbed', {
        label: Drupal.t('Embed Media'),
        command: 'SimpleMediaEmbed',
        toolbar: 'insert',
        icon: this.path + 'icons/ckeditor_simple_media_embed.png'
      });
    }
  });
})();
